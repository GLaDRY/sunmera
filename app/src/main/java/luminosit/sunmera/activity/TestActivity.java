package luminosit.sunmera.activity;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import luminosit.sunmera.R;
import luminosit.sunmera.util.DatabaseHelper;
import luminosit.sunmera.util.PhotoHandler;
import luminosit.sunmera.util.UserDatabase;

/*
* Created by 35 34 KR 36 38 KR 36 35 KR 32 30 KR 37 30 KR 36 35 KR
* 37 32 KR 37 33 KR 36 66 KR 36 65 KR 32 30 KR 37 37 KR
* 36 38 KR 36 66 KR 32 30 KR 36 63 KR 36 66 KR 37 36 KR
* 36 35 KR 37 33 KR 32 30 KR 35 34 KR 36 39 KR 36 65 KR
* 36 31 KR 32 30 KR 36 64 KR 36 66 KR 37 33 KR 37 34 on 3/6/15 :)
* */

/*
* NOTE: this activity is used for testing multi-users mode, 
* there may be a different way to add new users in future update.
* */

public class TestActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_test, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
/*
    public void save(View view){
        DatabaseHelper helper = new DatabaseHelper(this);
        String uid = getIDInput();

        helper.insertNewColumn(DatabaseHelper.COLUMN_UID, uid);
        helper.updateColumn(DatabaseHelper.COLUMN_TEXT, getTextInput(),
                DatabaseHelper.COLUMN_UID, uid);
    }

    public void retrieve(View view) {
        DatabaseHelper helper = new DatabaseHelper(this);

        print(helper.read(DatabaseHelper.COLUMN_TEXT,
                DatabaseHelper.COLUMN_UID,
                getIDInput()));
    }
*/
    public void addUser(View view){
        UserDatabase userDatabase = new UserDatabase(this);
        userDatabase.addUser(getIDInput());
    }

    public void onDeleteUserClicked(View view){
        deleteUser(getIDInput());
    }

    private void deleteUser(String user){
        DatabaseHelper helper = new DatabaseHelper(this);
        PhotoHandler handler = new PhotoHandler(this);
        UserDatabase userDatabase = new UserDatabase(this);

        String[] list = helper.getUIDListByUser(user);
        String[] posterList = helper.getUsersPoster(user);
        for(String item : list){
            //delete photo
            handler.deleteImage(item);
            handler.deleteImageThumbnail(item);
        }

        for(String item : posterList){
            //delete poster
            handler.deleteImage(item);
            handler.deleteImageThumbnail(item);
        }

        helper.deleteUser(user);
        userDatabase.deleteUser(user);
    }

    public void displayAllUsers(View view){
        UserDatabase userDatabase = new UserDatabase(this);
        printArray(userDatabase.getUserList());
    }

    public void extra(View view){
        UserDatabase userDatabase = new UserDatabase(this);

        String[] users = userDatabase.getUserList();
        for(String user : users){
            deleteUser(user);
        }

        userDatabase.deleteAll();
    }

    public String getIDInput(){
        return ((EditText)findViewById(R.id.input_uid)).getText().toString();
    }

    /*
    public String getTextInput(){
        return ((EditText)findViewById(R.id.input_text)).getText().toString();
    }
    */

    public void print(String string){
        ((TextView)findViewById(R.id.output)).setText(string);
    }

    public void printArray(String[] string){
        String text = "";
        for (int i = 0; i < string.length; i++){
            text += "," + string[i];
        }
        print(text);
    }
}

